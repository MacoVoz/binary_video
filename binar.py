import cv2

def bin(count):
    img = cv2.imread("/Users/viktor/PycharmProjects/binarization_ocy/%d.jpg" % count, 0)
    blur = cv2.GaussianBlur(img, (5, 5), 0)
    ret1, th1 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.imwrite("%d.jpg" % count, th1)
    # return th1