import time
from resolution import get_resolution
from frame import frames, length, bright
from binar import bin

start_time = time.time()

path='/Users/viktor/Desktop/small.mp4' #path to video

resolution=get_resolution(path)
width=resolution[0]
height=resolution[1]
y=int(height/2)
x=0
count=0
i=0
framerate=33
l=length(path)
# print(l)

while (count<l):
    frames(i,count,y,height,x,width,path)
    bright(3, count)
    bin_image=bin(count)
    #func that calculate angle
    count+=1
    i+=framerate
    # print(count,i)


print("--- %s seconds ---" % (time.time() - start_time))